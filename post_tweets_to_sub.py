import twitter
import time
import praw

# These can be retrieved from you twitter app
CONSUMER_KEY = ''
CONSUMER_SECRET = ''
ACCESS_TOKEN = ''
ACCESS_TOKEN_SECRET = ''

REDDIT_USERNAME = ''
REDDIT_PASS = ''
SUBREDDIT_NAME = ''

TWITTER_ID = ''
LINK_TEMPLATE = 'http://twitter.com/{}/status/{}'

def post(link, title, r, sub):
    try:
        r.submit(sub, title, text=None, url=link, captcha=None)
        print "Posted tweet."
    except praw.errors.AlreadySubmitted:
        print "Already submitted tweet, continuing..."

def getTweets(api, r, sub):
    # Change the number at the end of this line if you want it
    # to fetch more tweets
    statuses = api.GetUserTimeline(TWITTER_ID)[:10]
    for s in statuses:
        user = s.user.screen_name.encode('ascii', 'ignore')
        link = LINK_TEMPLATE.format(user, s.GetId())
        # Number at the end sets title limit
        title = s.text.encode('ascii', 'ignore')[:150]
        post(link, title, r, sub)


def main():
    r = praw.Reddit(user_agent = 'Twitter_Poster v0.1')
    r.login(REDDIT_USERNAME, REDDIT_PASS)
    sub = r.get_subreddit(SUBREDDIT_NAME)

    api = twitter.Api(consumer_key = CONSUMER_KEY,
                      consumer_secret = CONSUMER_SECRET,
                      access_token_key = ACCESS_TOKEN,
                      access_token_secret = ACCESS_TOKEN_SECRET)


    while True:
        print "Gettings tweets..."
        getTweets(api, r, sub)
        print "Sleeping..."
        # Change this if you want it to sleep longer, or shorter
        # Time is in seconds currently 30 minutes
        time.sleep(1800)

if __name__ == "__main__":
    main()
